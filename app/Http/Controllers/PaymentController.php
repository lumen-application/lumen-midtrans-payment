<?php

namespace App\Http\Controllers;

use Exception;
use GuzzleHttp\Client;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getPayment()
    {
        $client = new Client();
        try {
            $res = $client->request('GET', 'http://localhost:8000/snap', []);
            $data = json_decode($res->getBody()->getContents());
            return view('payment', ['token' => $data->token]);
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

}
